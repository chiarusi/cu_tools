#!/bin/bash

# DUBase Controller
#
# This script provides the shifter with a GUI
# for controlling the DUBase devices
#
# Author: Tommaso Chiarusi <tommaso.chiarusi@bo.infn.it>
#
#




WorkingDIR="/home/km3net/applications/cu_tools"

TMPLOG=${WorkingDIR}"/log/DUBase/.tmplog"
LOG=${WorkingDIR}"/log/DUBase/DUBase.log"  # /cance.log" 

if [ -e ${LOG} ]; then
    echo "LOG file present"
else
    touch ${LOG}
fi


function LOG_Update(){
data=`date`
echo "" > ${TMPLOG}
echo -e "${data}">> ${TMPLOG}
echo $1  >> ${TMPLOG}
cat ${LOG}>>${TMPLOG}
cat ${TMPLOG}>${LOG}

}

while [ 1 ]; do
    
    
    ARCADU=$(zenity  --list  --text "DU Base Controller: " \
	--radiolist  --width 400 --height=220 --column " " --column "DU" \
	FALSE "ARCA-DU1" \
	FALSE "ARCA-DU2" FALSE "ARCA-DU3" \
	TRUE  "Exit" \
	); 
    
    
    LOG_Update "Chosen to work with ${ARCADU}"
    
    
    BaseIP="0.0.0.0"
    ALLOWED_12V="false"
    DU="0"
    PING_STATUS="false"

    EDFA_AWK='$2'

    if [ "${ARCADU}" == "ARCA-DU1" ]; then
	DU="1"

    elif [ "${ARCADU}" == "ARCA-DU2" ]; then
	DU="2"
	ALLOWED_12V="true"
	
    elif [ "${ARCADU}" == "ARCA-DU3" ]; then
	DU="3"
	ALLOWED_12V="true"

    elif [ "${ARCADU}" == "Exit" ]; then
	exit 0
    fi
    
    BaseIP="10.0.${DU}.100"    
    
    BPSRead=${WorkingDIR}"/CU_tools/BPSRead.exe ${BaseIP}"
    
    BPSToggleBackbone=${WorkingDIR}"/CU_tools/BPSToggleBackbone.exe ${BaseIP}"
    BPSToggle12V=${WorkingDIR}"/CU_tools/BPSToggle12V.exe ${BaseIP}"
    
    EDFA_RAG=${WorkingDIR}"/CU_tools/EDFARead.exe ${BaseIP} rag"
    EDFA_RIP=${WorkingDIR}"/CU_tools/EDFARead.exe ${BaseIP} rip"
    EDFA_ROP=${WorkingDIR}"/CU_tools/EDFARead.exe ${BaseIP} rop"
    EDFA_STA=${WorkingDIR}"/CU_tools/EDFARead.exe ${BaseIP} sta"
    EDFA_RCT=${WorkingDIR}"/CU_tools/EDFARead.exe ${BaseIP} rct"
    
    
    SLEEPTIME=8
    
    
    while [ 1 ]; do
	
# choiche=`zenity --title "DU-Base Controller" \
#        --list \
#        --column="Operations" \
#        --height=300 \
#         "BPS sensors" \
#        "BPS toggle backbone" \
#        "BPS toggle 12V" \
#        "EDFA read status" \   
#        "EDFA read power info" \
#        "WR parameters (with DM on)"`
	
	choiche=`zenity --title "${ARCADU}-Base Controller" \
            --list \
            --column="Operations" \
            --height=430 \
	    --width=300 \
            "BPS sensors" \
            "EDFA read status" \
	    "EDFA read power info" \
	    "EDFA read temperature" \
	    "WR parameters (with DM on)" \
	    "BPS 12V switch ON/OFF" \
            "BPS backbone switch ON/OFF" \
	    " " \
	    "Ping/UnPing" \
	    " " \
	    "Change DU" `
	
	if [ "${choiche}" == "Change DU" ]; then
	    break
	    
	    
	elif [ "${choiche}" == "Ping/UnPing" ]; then
	    
	    if [ ${PING_STATUS} == "false"   ]; then
	    
		${WorkingDIR}/open-pingterm_par.sh ${DU}
		PING_STATUS="true"
	    else
		killall ping
		PING_STATUS="false"
	    fi
	else
	
	if [ "${choiche}" == "BPS sensors" ]; then
	    
	    data=`date`
	    echo "" > ${TMPLOG}
	    echo -e "${data}">> ${TMPLOG}
	    echo "${ARCADU}- Checking BPS sensors" >> ${TMPLOG}
	    mono $BPSRead >> ${TMPLOG}
	    cat ${LOG}>>${TMPLOG}
	    cat ${TMPLOG}>${LOG}
	    
#     mono $BPSRead
	    
#
	    
	elif [ "${choiche}" == "BPS 12V switch ON/OFF" ]; then
	    
	    	    
	    CurrentStatus="N/A";
	    TargetStatus="N/A";
#
	    Value=`mono $BPSRead | grep "I 12V " | awk '{print $4}'`

	    if [ ${Value} -gt 0 ]; then
		CurrentStatus="ON"
		TargetStatus="OFF"
	    elif  [ ${Value} -eq 0 ]; then
		CurrentStatus="OFF"
		TargetStatus="ON"
	    else
		echo "${ARCADU}- AB-Normal value for I Backbone: ${Value}"
	    fi

	    echo "${ARCADU}- Toggling Hydro 12V: Current status: ${CurrentStatus} - Target Status: ${TargetStatus} "
	    LOG_Update "${ARCADU}- Toggling Hydro 12V: Current status: ${CurrentStatus} - Target Status: ${TargetStatus} "

	    
	    if [ ${ALLOWED_12V} == "true"  ]; then


		
		zenity --question --title="${ARCADU} 12V" \
		    --text "${ARCADU}- Hydro 12V: ${CurrentStatus}; You are going to switch it ${TargetStatus}.\nAre you sure you want to proceed?" \
		    --ok-label="Yes" --cancel-label="No"
		
		if [ $? = 0 ] ; then
		    zenity --question --title="${ARCADU} - WARNING" \
			--text "${ARCADU}- Are you really sure?\nYou cannot stop this operation" \
			--ok-label="Yes" --cancel-label="No"
		    
		    if [ $? = 0 ] ; then
			
			echo "${ARCADU}- Toggling and then sleeping ${SLEEPTIME} seconds before notifying... be patient"
			data=`date`
			echo "" > ${TMPLOG}
			echo -e "${ARCADU}-  ${data}">> ${TMPLOG}
			echo "${ARCADU}- Toggling 12V line" >> ${TMPLOG}
			mono $BPSToggle12V >> ${TMPLOG}
			
			sleep ${SLEEPTIME}
			echo "${ARCADU}- Checking BPS sensors" >> ${TMPLOG}
			mono $BPSRead >> ${TMPLOG}
			cat ${LOG}>>${TMPLOG}
			cat ${TMPLOG}>${LOG}
			
		    else 
			echo "${ARCADU}- 12V power cycle aborted"
			LOG_Update  "${ARCADU}- DU 12V power cycle aborted"
		    fi
		else 
		    echo "${ARCADU}- 12V power cycle aborted"
		    LOG_Update  "${ARCADU}- DU 12V power cycle aborted"
		fi
	    else
		
		zenity --warning --title{"${ARCADU}- WARNING"} --text "WARNING: for ${ARCADU} this operation is not permitted!";
		LOG_Update  " ${ARCADU}- 12V cycle aborted;  it can't be done for ${ARCADU} "
	    fi
	    
	elif [ "${choiche}" == "BPS backbone switch ON/OFF" ]; then
	    
	    
	    CurrentStatus="N/A";
	    TargetStatus="N/A";
	    Value=`mono $BPSRead | grep "I BackBone" | awk '{print $3}'`
	    if [ $Value -gt 80 ]; then
		CurrentStatus="ON"
		TargetStatus="OFF"
	    elif  [ $Value -lt 10 ]; then
		CurrentStatus="OFF"
		TargetStatus="ON"
	    else
		echo "${ARCADU}- AB-Normal value for I Bacbone: ${Value}"
	    fi
	    
	    LOG_Update "${ARCADU}- Backbone Toggle: Current status: ${CurrentStatus} - Target Status: ${TargetStatus} "
	    
	    zenity --question --title="${ARCADU} backbone" \
		--text "${ARCADU}- Backbone ${CurrentStatus}; You are going to switch ${TargetStatus} the DU backbone.\nAre you sure you want to proceed?" \
		--ok-label="Yes" --cancel-label="No"
	    
	    if [ $? = 0 ] ; then
		zenity --question --title="${ARCADU} - WARNING" \
                    --text "${ARCADU}- Are you really sure?\nYou cannot stop this operation" \
                    --ok-label="Yes" --cancel-label="No"
		
		if [ $? = 0 ] ; then
		    
		    echo "${ARCADU}- Toggling and then sleeping ${SLEEPTIME} seconds before notifying... be patient"
		    data=`date`
		    echo "" > ${TMPLOG}
		    echo -e " ${ARCADU}- ${data}">> ${TMPLOG}
		    echo "${ARCADU}- Toggling backbone" >> ${TMPLOG}
		    mono $BPSToggleBackbone >> ${TMPLOG}
		    sleep ${SLEEPTIME}
		    echo "${ARCADU}- Checking BPS sensors" >> ${TMPLOG}
		    mono $BPSRead >> ${TMPLOG}
		    cat ${LOG}>>${TMPLOG}
		    cat ${TMPLOG}>${LOG}
		    
		else 
		    echo "${ARCADU}-  backbone power cycle aborted"
		fi
	    else 
		echo "${ARCADU}-  backbone power cycle aborted"
	    fi
	    
	    
	elif  [ "${choiche}" == "EDFA read status" ]; then
	    data=`date`
	    echo "" > ${TMPLOG}
	    echo -e " ${data}">> ${TMPLOG}
	    echo "${ARCADU}- Reading EDFA Status" >> ${TMPLOG}
	    mono $EDFA_STA > EDFA.tmp
	    status=`cat EDFA.tmp |xargs|awk "{print $EDFA_AWK}"`
	    echo "${ARCADU}- Status (decimal): "${status}   >> ${TMPLOG}
	    bin_status=`echo "obase=2;${status}"|bc`
	    echo "${ARCADU}- Status (binary): "${bin_status}   >> ${TMPLOG}
	    echo ""
#     echo "A: 0 = Device not in 3 secs startup; 1 = Device in 3secs startup"   >> ${LOG}
#     echo "B: 0 = Emission OFF; 1= Emission ON"   >> ${LOG}
	    
#Retrieving The Important Bits:
	    Bit2=`printf "%032s\n" "$(bc <<< "obase=2;100663332")"|sed -r 's/.{1}/ibase=2;&\n/g'|bc |xargs |rev |awk '{print $3}'`
	    Bit15=`printf "%032s\n" "$(bc <<< "obase=2;100663332")"|sed -r 's/.{1}/ibase=2;&\n/g'|bc |xargs |rev |awk '{print $16}'`
	    Status2=""
	    Status15=""
	    
	    if [ "${Bit2}" == "0" ]; then
		Status2="Emission OFF"
	    elif [ "${Bit2}" == "1" ]; then
		Status2="Emission ON"
	    else
		Status2="Unknown"
	    fi
	    
	    
	    if [ "${Bit15}" == "0" ]; then
		Status15="Regime period"
	    elif [ "${Bit15}" == "1" ]; then
		Status15="Startup period"
	    else
		Status15="Unknown"
	    fi
	    
	    echo "bit 2: 0 = Emission OFF; 1= Emission ON"   >> ${TMPLOG}
	    echo "bit 15 : 0 = Device not in 3 secs startup; 1 = Device in 3secs startup"   >> ${TMPLOG}
	    
	    
	    
	    echo "${ARCADU}- Emission status: " ${Status2}>>${TMPLOG}
	    echo "${ARCADU}- Device period: " ${Status15}>>${TMPLOG}
	    
	    
	    
	    rm EDFA.tmp
	    cat ${LOG}>>${TMPLOG}
	    cat ${TMPLOG}>${LOG}
	    
	    
	    
	elif  [ "${choiche}" == "EDFA read power info" ]; then
	    data=`date`
	    echo "" > ${TMPLOG}
	    echo -e " ${data}">> ${TMPLOG}
	    echo "${ARCADU}- Reading EDFA optical power info" >> ${TMPLOG}
	    
	    mono $EDFA_RIP > EDFA.tmp
	    rip=`cat EDFA.tmp |xargs|awk "{print $EDFA_AWK}"`
	    
	    mono $EDFA_ROP > EDFA.tmp
	    rop=`cat EDFA.tmp |xargs|awk "{print $EDFA_AWK}"`
	    
	    mono $EDFA_RAG > EDFA.tmp
	    Gain=`cat EDFA.tmp |xargs|awk "{print $EDFA_AWK}"` 
	    
	    echo "${ARCADU}- Composite Input  Power: "${rip}" dB" >>${TMPLOG}
	    echo "${ARCADU}- Composite Output Power: "${rop}" dB" >>${TMPLOG}
	    echo "${ARCADU}- Actual Gain:             "${Gain}" dB"   >> ${TMPLOG}
#     echo "Check Output - Input   :"${check}" dB"  >> ${LOG}
	    rm EDFA.tmp
	    
	    cat ${LOG}>>${TMPLOG}
	    cat ${TMPLOG}>${LOG}
	    
	elif  [ "${choiche}" == "EDFA read temperature" ]; then
	    data=`date`
	    echo "" > ${TMPLOG}
	    echo -e " ${data}">> ${TMPLOG}
	    echo "${ARCADU}- Reading EDFA temperature" >> ${TMPLOG}
	    
	    mono $EDFA_RCT > EDFA.tmp
	    temp=`cat EDFA.tmp |xargs|awk "{print $EDFA_AWK}"`
	    
	    echo "${ARCADU}- EDFA temperature: "${temp}" dec" >>${TMPLOG}
	    cat ${LOG}>>${TMPLOG}
	    cat ${TMPLOG}>${LOG}
	    
	    
# WR PART---
	elif [ "${choiche}" == "WR parameters (with DM on)" ]; then
	    
	    
	    data=`date`
	    echo "" > ${TMPLOG}
	    echo -e " ${data}">> ${TMPLOG}
	    echo "${ARCADU}- WR Parameters (only from an active DM with jollytoken)" >> ${TMPLOG}
	    
	    
	    wget --save-cookies=ck.txt --keep-session-cookies "http://127.0.0.1:1302/login?sectoken=jollyroger&usr=pirate"
#
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_st_gen/1/0" -O wr.txt
	    wr_status=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_st_ptp/1/0" -O wr.txt
	    wr_ptp=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_mu/1/0" -O wr.txt
	    wr_rtt=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_delta/1/0/0" -O wr.txt
	    wr_delta0=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_delta/1/0/1" -O wr.txt
	    wr_delta1=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_delta/1/0/2" -O wr.txt
	    wr_delta2=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_delta/1/0/3" -O wr.txt
	    wr_delta3=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Bitslide/1/0" -O wr.txt
	    wr_bitslide=`cat wr.txt | grep value | awk '{print $5}'`
	    wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/wr_st_serv/1/0" -O wr.txt
	    wr_servo=`cat wr.txt | grep value | awk '{print $5}'`
	    
	    WR_STATUS=""
# status
	    if [ ${wr_status} == '"0"' ]; then
		WR_STATUS="PRESENT"
	    elif [ ${wr_status} == '"1"' ]; then
		WR_STATUS="S_LOCK"
	    elif [ ${wr_status} == '"2"' ]; then
		WR_STATUS="M_LOCK"
	    elif [ ${wr_status} == '"3"' ]; then
		WR_STATUS="LOCKED"
	    elif [ ${wr_status} == '"4"' ]; then
		WR_STATUS="CALIBRATION"
	    elif [ ${wr_status} == '"5"' ]; then
		WR_STATUS="CALIBRATED"
	    elif [ ${wr_status} == '"6"' ]; then
		WR_STATUS="RESP_CALIB_REQ"
	    elif [ ${wr_status} == '"7"' ]; then
		WR_STATUS="WR LINK ON"
	    elif [ ${wr_status} == '"8"' ]; then
		WR_STATUS="TIMER ARRAY SIZE"
	    elif [ ${wr_status} == '"9"' ]; then
		WR_STATUS="IDLE"
	    elif [ ${wr_status} == '"10"' ]; then
		WR_STATUS="S_LOCK_1"
	    elif [ ${wr_status} == '"11"' ]; then
		WR_STATUS="S_LOCK 2"
	    elif [ ${wr_status} == '"12"' ]; then
		WR_STATUS="GEN_CALIB_1"
	    elif [ ${wr_status} == '"13"' ]; then
		WR_STATUS="GEN_CALIB_2"
	    elif [ ${wr_status} == '"14"' ]; then
		WR_STATUS="GEN_CALIB_3"
	    elif [ ${wr_status} == '"15"' ]; then
		WR_STATUS="GEN_CALIB_4"
	    elif [ ${wr_status} == '"16"' ]; then
		WR_STATUS="GEN_CALIB_5"
	    elif [ ${wr_status} == '"17"' ]; then
		WR_STATUS="GEN_CALIB_6"
	    elif [ ${wr_status} == '"18"' ]; then
		WR_STATUS="GEN_CALIB_7"
	    elif [ ${wr_status} == '"19"' ]; then
		WR_STATUS="GEN_CALIB_8"
	    elif [ ${wr_status} == '"20"' ]; then
		WR_STATUS="GEN_RESP_CALIB_REQ_1"
	    elif [ ${wr_status} == '"21"' ]; then
		WR_STATUS="GEN_RESP_CALIB_REQ_2"
	    elif [ ${wr_status} == '"22"' ]; then
		WR_STATUS="GEN_RESP_CALIB_REQ_3"
	    elif [ ${wr_status} == '"-1"' ]; then
		WR_STATUS="UNINITIALIZED"
		
	    fi
	    
#  PTP
	    WR_PTP=""
	    if [ ${wr_ptp} == '"0"' ]; then
		WR_PTP="INIT"
	    elif [ ${wr_ptp} == '"1"' ]; then
		WR_PTP="FAULTY"
	    elif [ ${wr_ptp} == '"2"' ]; then
		WR_PTP="DISABLED"
	    elif [ ${wr_ptp} == '"3"' ]; then
		WR_PTP="LISTENING"
	    elif [ ${wr_ptp} == '"4"' ]; then
		WR_PTP="PRE_MASTER"
	    elif [ ${wr_ptp} == '"5"' ]; then
		WR_PTP="MASTER"
	    elif [ ${wr_ptp} == '"6"' ]; then
		WR_PTP="PASSIVE"
	    elif [ ${wr_ptp} == '"7"' ]; then
		WR_PTP="UNCALIBRATED"
	    elif [ ${wr_ptp} == '"8"' ]; then
		WR_PTP="SLAVE"
	    elif [ ${wr_ptp} == '"-1"' ]; then
		WR_PTP="UNINITIALIZED"
		
		
	    fi
	    
# SERVO
	    WR_SERVO=""
	    
	    if [ ${wr_servo} == '"0"' ]; then
		WR_SERVO="UNKNOWN"
	    elif [ ${wr_servo} == '"1"' ]; then
		WR_SERVO="SYNC_NSEC"
	    elif [ ${wr_servo} == '"2"' ]; then
		WR_SERVO="SYNC_TAI"
	    elif [ ${wr_servo} == '"3"' ]; then
		WR_SERVO="SYNC_PHASE"
	    elif [ ${wr_servo} == '"4"' ]; then
		WR_SERVO="TRACK_PHASE"
	    elif [ ${wr_servo} == '"5"' ]; then
		WR_SERVO="WAIT_SYNC_IDLE"
	    elif [ ${wr_servo} == '"6"' ]; then
		WR_SERVO="WAIT_OFFSET_STABLE"
	    elif [ ${wr_servo} == '"-1"' ]; then
		WR_SERVO="UNINITIALIZED"
	    fi
	    
	    
	    echo "${ARCADU}- Status: "${WR_STATUS} >>${TMPLOG}
	    echo "${ARCADU}- PTP: "${WR_PTP} >>${TMPLOG}
	    echo "${ARCADU}- Servo: "${WR_SERVO} >>${TMPLOG}
	    
	    echo "${ARCADU}- Bitslide: "${wr_bitslide}" ps " >>${TMPLOG}
	    echo "${ARCADU}- Round Trip Time: "${wr_rtt}" ps " >>${TMPLOG}
	    
	    
	    echo "${ARCADU}- Master Delays (Tx,Rx): "${wr_delta2}" ps   -  "${wr_delta3}" ps ">>${TMPLOG}
	    echo "${ARCADU}- Slave  Delays (Tx,Rx): "${wr_delta0}" ps   -  "${wr_delta1}" ps ">>${TMPLOG}
	    
	    
	    
     #rm -f ck.txt wr.txt
	    
	    rm -f login\?sectoken\=jollyroger\&usr\=pirate*
	    cat ${LOG}>>${TMPLOG}
	    cat ${TMPLOG}>${LOG}
	fi
	
	
	zenity --text-info \
            --width=650 \
            --height=400 \
            --filename ${LOG} \
	    
	fi	    
    done
    
    
done