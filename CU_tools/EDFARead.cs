﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPSRead
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("usage EDFARead.exe <IP> <cmd>");
                return;
            }
            try
            {
                org.km3net.clbv2.remote.Control ctl = new org.km3net.clbv2.remote.Control(args[0]);
                Console.WriteLine(ctl.bse.dbgEdfaCmdReply(args[1]).get());
                Console.WriteLine("Closing");
                ctl.close();
                Console.WriteLine("Closed");
            }
            catch (Exception xc)
            {
                if (xc.ToString().IndexOf("12403") < 0)
                  Console.WriteLine(xc.ToString());
            }
            java.lang.System.exit(0);
        }
    }
}
