﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPSRead
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                org.km3net.clbv2.remote.Control ctl = new org.km3net.clbv2.remote.Control(args[0]);
                System.IO.MemoryStream ms = new System.IO.MemoryStream((byte[])ctl.bse.dbgBpsCmdReply(0x03, null, 0x04, 32).get());
                string[] label = new string[]{"I BackBone:\t","I 12V (Hydro):\t","V 375VDC:\t","spare:\t","I 375V:\t","V 5VDC:\t","Breaker status:\t","Alarm status:\t" };
                byte[] chars = new byte[4];
                int icount =0;
                while (ms.Position < ms.Length)
                {
                    ms.Read(chars, 0, 4);
                    Array.Reverse(chars);
                    int value = Convert.ToInt32(System.Text.Encoding.ASCII.GetString(chars), 16);
                    Console.WriteLine(label[icount] + value + " (" + value.ToString("X08") + ")");
                    icount ++;
                }
                Console.WriteLine("Closing");
                ctl.close();
                Console.WriteLine("Closed");
            }
            catch (Exception xc)
            {
                Console.WriteLine(xc.ToString());
            }
            java.lang.System.exit(0);
        }
    }
}
