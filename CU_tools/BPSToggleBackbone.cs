﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPSRead
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                org.km3net.clbv2.remote.Control ctl = new org.km3net.clbv2.remote.Control(args[0]);
                System.IO.MemoryStream ms = new System.IO.MemoryStream((byte[])ctl.bse.dbgBpsCmdReply(0x05, null, 0x06, 4).get());
               byte[] charoffs = new byte[] { 48, 48, 48, 48 };
                while (ms.Position < ms.Length)
                {
                    int value = charoffs.Select((x, i) => ((int)(ms.ReadByte() - x)) << (4 * i)).Aggregate((a, b) => a + b);
                   Console.WriteLine("Data is " + value + " (" + value.ToString("X08") + ")");
                }
                Console.WriteLine("Closing");
                ctl.close();
                Console.WriteLine("Closed");
            }
            catch (Exception xc)
            {
///                Console.WriteLine(xc.ToString());
            }
            java.lang.System.exit(0);
        }
    }
}
