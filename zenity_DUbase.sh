#!/bin/bash

# DUBase Controller
#
# This script provides the shifter with a GUI
# for controlling the DUBase devices
#
# Author: Tommaso Chiarusi <tommaso.chiarusi@bo.infn.it>
#
#
WorkingDIR="/home/km3net/applications/cu_tools"

BPSRead=${WorkingDIR}"/CU_tools/BPSRead.exe 10.0.1.100"
BPSToggleBackbone=${WorkingDIR}"/CU_tools/BPSToggleBackbone.exe 10.0.1.100"
BPSToggle12V=${WorkingDIR}"/CU_tools/BPSToggle12V.exe 10.0.1.100"

EDFA_RAG=${WorkingDIR}"/CU_tools/EDFARead.exe 10.0.1.100 rag"
EDFA_RIP=${WorkingDIR}"/CU_tools/EDFARead.exe 10.0.1.100 rip"
EDFA_ROP=${WorkingDIR}"/CU_tools/EDFARead.exe 10.0.1.100 rop"
EDFA_STA=${WorkingDIR}"/CU_tools/EDFARead.exe 10.0.1.100 sta"

SLEEPTIME=8

TMPLOG=${WorkingDIR}"/log/DUBase/.tmplog"
LOG=${WorkingDIR}"/log/DUBase/DUBase.log"

if [ -e ${LOG} ]; then
    echo "LOG file present"
else
    touch ${LOG}
fi

while [ 1 ]; do

# choiche=`zenity --title "DU-Base Controller" \
#        --list \
#        --column="Operations" \
#        --height=300 \
#         "BPS sensors" \
#        "BPS toggle backbone" \
#        "BPS toggle 12V" \
#        "EDFA read status" \
#        "EDFA read power info" \
#        "WR parameters (with DM on)"`

 choiche=`zenity --title "DU-Base Controller" \
        --list \
        --column="Operations" \
        --height=300 \
         "BPS sensors" \
        "BPS toggle backbone" \
        "EDFA read status" \
        "EDFA read power info" \
        "WR parameters (with DM on)"`

        

 if [ "${choiche}" == "BPS sensors" ]; then

     data=`date`
     echo "" > ${TMPLOG}
     echo -e "${data}">> ${TMPLOG}
     echo "Checking BPS sensors" >> ${TMPLOG}
     mono $BPSRead >> ${TMPLOG}
     cat ${LOG}>>${TMPLOG}
     cat ${TMPLOG}>${LOG}

#     mono $BPSRead

#

 elif [ "${choiche}" == "BPS toggle backbone" ]; then

     echo "Toggling and then sleeping ${SLEEPTIME} before notifying... be patient"
     data=`date`
     echo "" > ${TMPLOG}
     echo -e " ${data}">> ${TMPLOG}
     echo "Toggling backbone" >> ${TMPLOG}
     mono $BPSToggleBackbone >> ${TMPLOG}
     sleep ${SLEEPTIME}
     echo "Checking BPS sensors" >> ${TMPLOG}
     mono $BPSRead >> ${TMPLOG}
     cat ${LOG}>>${TMPLOG}
     cat ${TMPLOG}>${LOG}

#   elif [ "${choiche}" == "BPS toggle 12V" ]; then
#  
#       echo "Toggling and then sleeping ${SLEEPTIME} before notifying... be patient"
#       data=`date`
#       echo "" > ${TMPLOG}
#       echo -e " ${data}">> ${TMPLOG}
#       echo "Toggling 12V line" >> ${TMPLOG}
#       mono $BPSToggle12V >> ${TMPLOG}
#  
#       sleep ${SLEEPTIME}
#       echo "Checking BPS sensors" >> ${TMPLOG}
#       mono $BPSRead >> ${TMPLOG}
#     cat ${LOG}>>${TMPLOG}
#       cat ${TMPLOG}>${LOG}

 elif  [ "${choiche}" == "EDFA read status" ]; then
     data=`date`
     echo "" > ${TMPLOG}
     echo -e " ${data}">> ${TMPLOG}
     echo "Reading EDFA Status" >> ${TMPLOG}
     mono $EDFA_STA > EDFA.tmp
     status=`cat EDFA.tmp |xargs|awk '{print $1}'`
       echo "Status (decimal): "${status}   >> ${TMPLOG}
     bin_status=`echo "obase=2;${status}"|bc`
     echo "Status (binary): "${bin_status}   >> ${TMPLOG}
     echo ""
#     echo "A: 0 = Device not in 3 secs startup; 1 = Device in 3secs startup"   >> ${LOG}
#     echo "B: 0 = Emission OFF; 1= Emission ON"   >> ${LOG}

#Retrieving The Important Bits:
     Bit2=`printf "%032s\n" "$(bc <<< "obase=2;100663332")"|sed -r 's/.{1}/ibase=2;&\n/g'|bc |xargs |rev |awk '{print $3}'`
     Bit15=`printf "%032s\n" "$(bc <<< "obase=2;100663332")"|sed -r 's/.{1}/ibase=2;&\n/g'|bc |xargs |rev |awk '{print $16}'`
     Status2=""
     Status15=""

     if [ "${Bit2}" == "0" ]; then
        Status2="Emission OFF"
     elif [ "${Bit2}" == "1" ]; then
       Status2="Emission ON"
     else
       Status2="Unknown"
     fi


    if [ "${Bit15}" == "0" ]; then
        Status15="Regime period"
     elif [ "${Bit15}" == "1" ]; then
       Status15="Startup period"
     else
       Status15="Unknown"
     fi

     echo "bit 2: 0 = Emission OFF; 1= Emission ON"   >> ${TMPLOG}
     echo "bit 15 : 0 = Device not in 3 secs startup; 1 = Device in 3secs startup"   >> ${TMPLOG}



     echo "Emission status: " ${Status2}>>${TMPLOG}
     echo "Device period: " ${Status15}>>${TMPLOG}



     rm EDFA.tmp


 elif  [ "${choiche}" == "EDFA read power info" ]; then
     data=`date`
     echo "" >> ${TMPLOG}
     echo -e " ${data}">> ${TMPLOG}
     echo "Reading EDFA optical power info" >> ${TMPLOG}

     mono $EDFA_RIP > EDFA.tmp
     rip=`cat EDFA.tmp |xargs|awk '{print $1}'`

     mono $EDFA_ROP > EDFA.tmp
     rop=`cat EDFA.tmp |xargs|awk '{print $1}'`

     mono $EDFA_RAG > EDFA.tmp
     Gain=`cat EDFA.tmp |xargs|awk '{print $1}'`

     echo "Composite Input  Power :"${rip}" dB" >>${TMPLOG}
     echo "Composite Output Power :"${rop}" dB" >>${TMPLOG}
     echo "Actual Gain            :"${Gain}" dB"   >> ${TMPLOG}
#     echo "Check Output - Input   :"${check}" dB"  >> ${LOG}
     rm EDFA.tmp

     cat ${LOG}>>${TMPLOG}
     cat ${TMPLOG}>${LOG}


# WR PART---
 elif [ "${choiche}" == "WR parameters (with DM on)" ]; then


     data=`date`
     echo "" > ${TMPLOG}
     echo -e " ${data}">> ${TMPLOG}
     echo "WR Parameters (only from an active DM with jollytoken)" >> ${TMPLOG}
     

     wget --save-cookies=ck.txt --keep-session-cookies "http://127.0.0.1:1302/login?sectoken=jollyroger&usr=pirate"
#
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Status/1/0" -O wr.txt
     wr_status=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_PTP/1/0" -O wr.txt
     wr_ptp=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_RoundTripTime/1/0" -O wr.txt
     wr_rtt=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Deltas/1/0/0" -O wr.txt
     wr_delta0=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Deltas/1/0/1" -O wr.txt
     wr_delta1=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Deltas/1/0/2" -O wr.txt
     wr_delta2=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Deltas/1/0/3" -O wr.txt
     wr_delta3=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Bitslide/1/0" -O wr.txt
     wr_bitslide=`cat wr.txt | grep value | awk '{print $5}'`
     wget --load-cookies=ck.txt "http://127.0.0.1:1302/mon/clb/outparams/WR_Servo/1/0" -O wr.txt
     wr_servo=`cat wr.txt | grep value | awk '{print $5}'`

    WR_STATUS=""
# status
    if [ ${wr_status} == "0" ]; then
	WR_STATUS="PRESENT"
    elif [ ${wr_status} == "1" ]; then
	WR_STATUS="S_LOCK"
    elif [ ${wr_status} == "2" ]; then
	WR_STATUS="M_LOCK"
    elif [ ${wr_status} == "3" ]; then
	WR_STATUS="LOCKED"
    elif [ ${wr_status} == "4" ]; then
	WR_STATUS="CALIBRATION"
    elif [ ${wr_status} == "5" ]; then
	WR_STATUS="CALIBRATED"
    elif [ ${wr_status} == "6" ]; then
	WR_STATUS="RESP_CALIB_REQ"
    elif [ ${wr_status} == "7" ]; then
	WR_STATUS="WR LINK ON"
    elif [ ${wr_status} == "8" ]; then
	WR_STATUS="TIMER ARRAY SIZE"
    elif [ ${wr_status} == "9" ]; then
	WR_STATUS="IDLE"
    elif [ ${wr_status} == "10" ]; then
	WR_STATUS="S_LOCK_1"
    elif [ ${wr_status} == "11" ]; then
	WR_STATUS="S_LOCK 2"
    elif [ ${wr_status} == "12" ]; then
	WR_STATUS="GEN_CALIB_1"
    elif [ ${wr_status} == "13" ]; then
	WR_STATUS="GEN_CALIB_2"
    elif [ ${wr_status} == "14" ]; then
	WR_STATUS="GEN_CALIB_3"
    elif [ ${wr_status} == "15" ]; then
	WR_STATUS="GEN_CALIB_4"
    elif [ ${wr_status} == "16" ]; then
	WR_STATUS="GEN_CALIB_5"
    elif [ ${wr_status} == "17" ]; then
	WR_STATUS="GEN_CALIB_6"
    elif [ ${wr_status} == "18" ]; then
	WR_STATUS="GEN_CALIB_7"
    elif [ ${wr_status} == "19" ]; then
	WR_STATUS="GEN_CALIB_8"
    elif [ ${wr_status} == "20" ]; then
	WR_STATUS="GEN_RESP_CALIB_REQ_1"
    elif [ ${wr_status} == "21" ]; then
	WR_STATUS="GEN_RESP_CALIB_REQ_2"
    elif [ ${wr_status} == "22" ]; then
	WR_STATUS="GEN_RESP_CALIB_REQ_3"
    elif [ ${wr_status} == "-1" ]; then
	WR_STATUS="UNINITIALIZED"

    fi

#  PTP
    WR_PTP=""
    if [ ${wr_ptp} == "0" ]; then
	WR_PTP="INIT"
    elif [ ${wr_ptp} == "1" ]; then
	WR_PTP="FAULTY"
    elif [ ${wr_ptp} == "2" ]; then
	WR_PTP="DISABLED"
    elif [ ${wr_ptp} == "3" ]; then
	WR_PTP="LISTENING"
    elif [ ${wr_ptp} == "4" ]; then
	WR_PTP="PRE_MASTER"
    elif [ ${wr_ptp} == "5" ]; then
	WR_PTP="MASTER"
    elif [ ${wr_ptp} == "6" ]; then
	WR_PTP="PASSIVE"
    elif [ ${wr_ptp} == "7" ]; then
	WR_PTP="UNCALIBRATED"
    elif [ ${wr_ptp} == "8" ]; then
	WR_PTP="SLAVE"
    elif [ ${wr_ptp} == "-1" ]; then
	WR_PTP="UNINITIALIZED"


    fi

# SERVO
    WR_SERVO=""

    if [ ${wr_servo} == "0" ]; then
	WR_SERVO="UNKNOWN"
    elif [ ${wr_servo} == "1" ]; then
	WR_SERVO="SYNC_NSEC"
    elif [ ${wr_servo} == "2" ]; then
	WR_SERVO="SYNC_TAI"
    elif [ ${wr_servo} == "3" ]; then
	WR_SERVO="SYNC_PHASE"
    elif [ ${wr_servo} == "4" ]; then
	WR_SERVO="TRACK_PHASE"
    elif [ ${wr_servo} == "5" ]; then
	WR_SERVO="WAIT_SYNC_IDLE"
    elif [ ${wr_servo} == "6" ]; then
	WR_SERVO="WAIT_OFFSET_STABLE"
    elif [ ${wr_servo} == "-1" ]; then
	WR_SERVO="UNINITIALIZED"
    fi


     echo "Status: "${WR_STATUS} >>${TMPLOG}
     echo "PTP: "${WR_PTP} >>${TMPLOG}
     echo "Servo: "${WR_SERVO} >>${TMPLOG}
 
     echo "Bitslide: "${wr_bitslide}" ps " >>${TMPLOG}
     echo "Round Trip Time: "${wr_rtt}" ps " >>${TMPLOG}
 

     echo "Master Delays (Tx,Rx): "${wr_delta2}" ps   -  "${wr_delta3}" ps ">>${TMPLOG}
     echo "Slave  Delays (Tx,Rx): "${wr_delta0}" ps   -  "${wr_delta1}" ps ">>${TMPLOG}



     rm -f ck.txt wr.txt
     
     rm -f login\?sectoken\=jollyroger\&usr\=pirate*
     cat ${LOG}>>${TMPLOG}
     cat ${TMPLOG}>${LOG}
 fi

     zenity --text-info \
            --width=650 \
            --height=400 \
            --filename ${LOG} \

done
