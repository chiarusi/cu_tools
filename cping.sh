#!/bin/bash



function coloredping() { 
  /bin/ping -s 1000 ${*} | \
  while read line; do
    res=`echo ${line} | grep ttl 2>/dev/null`
    if [ -n "${res}" ]; then
      echo -e "\x1b[34;42m${line}\033[0m"
    else
      echo -e "\x1b[93;41m${line}\033[0m"
    fi
  done 
}


address=$1

coloredping $address

