#!/bin/bash

Pprof=--window-with-profile=PingWindow
zoom="--zoom=1"


script_dir="/home/km3net/applications/cu_tools"




xoff=0
yoff=28
step=100

i=1
while [ $i -lt 19  ]; do
    
    if [ $i == 10 ]; then
	yoff=28
	xoff=900
    fi
	

    IP=`printf "10.0.1.1%02d" $i `

    title="--title=DOM${i}"
    geoq=--geometry=74x5+$xoff+$yoff
    gnome-terminal  $Pprof $title $zoom $geoq  -x sh -c " /home/km3net/applications/cu_tools/cping.sh $IP" &


yoff=`expr $yoff + $step`    
i=`expr $i + 1`

done

